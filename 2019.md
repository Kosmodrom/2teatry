##Piątek 24 maja

Sala nr 2

godz. 9:30 - Patrycja Dołowy "Matki", reż. Paweł Passini (120 min.)

godz. 12 - Jerzy Pilch "Zabijanie Gomułki", reż. Jacek Głomb (122 min.)

godz. 14:15 - Tomasz Śpiewak "Wszystko o mojej matce", reż. Michał Borczuch (123 min.)

godz. 16:45 - Molier "Chory z urojenia", reż. Giovanni Pampiglione (104 min.)

godz. 19 - Agnieszka Lipiec-Wróblewska "Fryderyk", reż. Agnieszka Lipiec-Wróblewska (83 min.)

godz. 21 - Stanisław Wyspiański "Wesele", reż. Wawrzyniec Kostrzewski (106 min.) - OTWARCIE FESTIWALU

Sala nr 3

godz. 10 - Jan Brzechwa "Pchła Szachrajka", reż. Anna Seniuk (64 min.)

godz. 11:15 - Julian Tuwim "Kwiaty polskie", reż. Jan Englert (53 min.)

godz. 12:30 - Jarosław Jakubowski "Generał", reż. Aleksandra Popławska, Marek Kalita (85 min.)

godz. 14:15 - Konrad Szczebiot, Janina Żukiewicz, Jan Nowara "Popiełuszko", reż. Jan Nowara (72 min.)

godz. 15:45 - Ernest Bryll "Wieczernik", reż. Krzysztof Tchórzewski (61 min.)

godz. 17 - Paweł Derewenda, Jakub Pączek "Reytan. Druga strona drzwi", reż. Jakub Pączek (70 min.)

godz. 18:30 - Wiktor Szenderowicz "Ekspedycja", reż. Wojciech Adamczyk (83 min.)

godz. 20:15 - Stanisław Grochowiak "Chłopcy", reż. Robert Gliński (73 min.)

##Sobota 25 maja

Sala nr 2

godz. 9 - Ernest Bryll "Wieczernik", reż. Krzysztof Tchórzewski (61 min.)

godz. 10:30 - Jarosław Jakubowski "Generał", reż. Aleksandra Popławska, Marek Kalita (85 min.)

godz. 12:15 - Paweł Derewenda, Jakub Pączek "Reytan. Druga strona drzwi", reż. Jakub Pączek (70 min.)

godz. 13:45 - Konrad Szczebiot, Janina Żukiewicz, Jan Nowara "Popiełuszko", reż. Jan Nowara (72 min.)

godz. 15:30 - Paweł Woldan "Prymas Hlond", reż. Paweł Woldan (79 min.)

godz. 17:15 - Andrzej Strzelecki "Paradiso", reż. Andrzej Strzelecki (84 min.)

godz. 19 - Janusz Krasiński "Czapa, czyli śmierć na raty", reż. Zbigniew Lesień (80 min.)

godz. 20:45 - Waldemar Łysiak "Cena", reż. Jerzy Zelnik (83 min.)

Sala nr 3

godz. 10 - Molier "Chory z urojenia", reż. Giovanni Pampiglione (104 min.)

godz. 11:45 - Redbad Klynstra-Komarnicki "Ziemia uderzyła o piłkę", reż. Grzegorz Jankowski (60 min.)

godz. 13 - Jarosław Jakubowski "Znaki", reż. Artur Tyszkiewicz (61 min.)

godz. 14:15 - Marek Bukowski, Maciej Dancewicz - "Dołęga-Mostowicz. Kiedy zamykam oczy", reż. Marek Bukowski (75 min.)

godz. 15:45 - Kornel Makuszyński "List z tamtego świata", reż. Anna Wieczur-Bluszcz (84 min.)

godz. 17:30 - Agnieszka Lipiec-Wróblewska "Fryderyk", reż. Agnieszka Lipiec-Wróblewska (83 min.)

godz. 19:15 - Tadeusz Ritter "Lato", reż. Jan Englert (72 min.)

godz. 20:45 - Remigiusz Grzela "Oriana Fallaci. Chwila, w której umarłam", reż. Ewa Błaszczyk (55 min.)

##Niedziela 26 maja

Sala nr 2

godz. 9 - Jan Brzechwa "Pchła Szachrajka", reż. Anna Seniuk (64 min.)

godz. 10:15 - Wiktor Szenderowicz "Ekspedycja", reż. Wojciech Adamczyk (83 min.)

godz. 12 - Kornel Makuszyński "List z tamtego świata", reż. Anna Wieczur-Bluszcz (84 min.)

godz. 13:45 - Julian Tuwim "Kwiaty polskie", reż. Jan Englert (53 min.)

godz. 15 - Tadeusz Ritter "Lato", reż. Jan Englert (72 min.)

godz. 16:30 - Remigiusz Grzela "Oriana Fallaci. Chwila, w której umarłam", reż. Ewa Błaszczyk (55 min.)

godz. 18 - Marek Bukowski, Maciej Dancewicz - "Dołęga-Mostowicz. Kiedy zamykam oczy", reż. Marek Bukowski (75 min.)

godz. 19:45 - Stanisław Grochowiak "Chłopcy", reż. Robert Gliński (73 min.)

godz. 21:30 - Redbad Klynstra-Komarnicki "Ziemia uderzyła o piłkę", reż. Grzegorz Jankowski (60 min.)

Sala nr 3

godz. 10 - Tomasz Drozdowicz, Beata Hyczko "Człowiek, który zatrzymał Rosję", reż. Tomasz Drozdowicz (87 min.)

godz. 11:45 - Paweł Woldan "Prymas Hlond", reż. Paweł Woldan (79 min.)

godz. 13:15 - Stanisław Wyspiański "Wesele", reż. Wawrzyniec Kostrzewski (106 min.)

godz. 15 - Andrzej Strzelecki "Paradiso", reż. Andrzej Strzelecki (84 min.)

godz. 16:45 - Waldemar Łysiak "Cena", reż. Jerzy Zelnik (83 min.)

godz. 18:15 - Włodzimierz Perzyński "Aszantka", reż. Jarosław Tumidajski (81 min.)

godz. 20 - Jewgienij Griszkowiec "Jednocześnie", reż. Artur Tyszkiewicz (54 min.)

21:45 - Tomasz Śpiewak "Wszystko o mojej matce", reż. Michał Borczuch (123 min.)

##Poniedziałek 27 maja

Sala nr 2

godz. 9:30 - Krzysztof Magowski "Ostatni hrabia. Edward Bernard Raczyński", reż. Krzysztof Magowski (79 min.)

godz. 11 - Włodzimierz Perzyński "Aszantka", reż. Jarosław Tumidajski (81 min.)

godz. 12:45 - Jewgienij Griszkowiec "Jednocześnie", reż. Artur Tyszkiewicz (54 min.)

godz. 14 - Tomasz Drozdowicz, Beata Hyczko "Człowiek, który zatrzymał Rosję", reż. Tomasz Drozdowicz (87 min.)

godz. 16 - Jarosław Jakubowski "Znaki", reż. Artur Tyszkiewicz (61 min.)

Sala nr 3

godz. 10 - Jerzy Pilch "Zabijanie Gomułki", reż. Jacek Głomb (122 min.)

godz. 12 - Krzysztof Magowski "Ostatni hrabia. Edward Bernard Raczyński", reż. Krzysztof Magowski (79 min.)

godz. 13:45 - Janusz Krasiński "Czapa, czyli śmierć na raty", reż. Zbigniew Lesień (80 min.)

godz. 15:30 - Patrycja Dołowy "Matki", reż. Paweł Passini (120 min.)

*Teatr Telewizji Polskiej
Sala nr 2 - prezentacje konkursowe w kategorii: Spektakl telewizyjny
Prowadzenie Urszula Matysiak i Bogdan Szczepański, Telewizja Gdańsk

Sala nr 3 - prezentacje w kategorii: Spektakl telewizyjny 